var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('html', function (){
	gulp.src(['./html/*.html', './ice-cream/*html'])
	.pipe(gulp.dest('public/html'))
	.pipe(browserSync.stream());
});

gulp.task('css', function (){
	gulp.src('./themes/css/*.css')
	.pipe(gulp.dest('public/themes/css'))
	.pipe(browserSync.stream());
});

gulp.task('font', function(){
	gulp.src(['./themes/font/*.css'])
	.pipe(gulp.dest('public/themes/font'))
	gulp.src(['./themes/Awesome/**'])
	.pipe(gulp.dest('public/themes/Awesome'))
	gulp.src(['./themes/ProductSans/**'])
	.pipe(gulp.dest('public/themes/ProductSans'))
});

gulp.task('image', function (){
	gulp.src([
		'./themes/image/*.jpg', 
		'./themes/image/*.png',
		'./themes/image/*.jpeg'	
		])
	.pipe(gulp.dest('public/themes/image'));
});

//

gulp.task('browserSync',['html','css',], function() {
  browserSync.init({
    server: {
      baseDir: 'public'
    },
  });
	
	gulp.watch('./themes/css/*.css', ['css'])
	gulp.watch(['./html/*.html', './ice-cream/*.html'], ['html']);
});

// gulp.task('sync', function() {
//   return gulp.src('./themes/css/*.css')
//     .pipe(sync())
//     .pipe(gulp.dest('public/themes/css'))
//     .pipe(browserSync.reload({
//       stream: true
//     }))
// });

gulp.task('default', ['browserSync', 'image','font']);